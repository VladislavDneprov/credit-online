<?php
/**
 * Шаблон отдельной записи (single.php)
 * 
 * 
 */
get_header(); ?>
<div class="container">
		<?php get_template_part('part/breadcrumbs'); ?>
        <div class="row">
            <div class="info-container">
            	<?php if ( have_posts() ) while ( have_posts() ) : the_post();?>
						<div class="img-single-container">
							<?=get_the_post_thumbnail(null,array(150,150)); ?>
						</div>
						<div class="text-justify">
							<h1 class="text-left"><?php the_title(); // заголовок поста ?></h1>
							<?=the_content(); ?>							
						</div>
				<?php endwhile;?>
                <div class="clear"></div>
            </div>
        </div>
		<div class="row">
			<?php get_kambeker(); ?>
		</div>

	</div>


<?php get_footer(); ?> 
