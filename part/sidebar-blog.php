<?php

$blog_category = get_category_by_slug('blog');
$current_category = get_query_var('cat');

$args = array(
	'type'         => 'post',
	'child_of'     => $blog_category->term_id,
	'orderby'      => 'name',
	'order'        => 'ASC',
	'hide_empty'   => 0,
	'exclude'      => '',
	'include'      => '',
	'number'       => 0,
	'taxonomy'     => 'category',
	'pad_counts'   => false,
);

$categories = get_categories($args);

?>

<!-- display category -->
<ul>
	<?php foreach ($categories as $key => $category): ?>
		<li <?php echo ($current_category == $category->term_id ? 'class="active"' : ''); ?>>
			<a href="<?= get_category_link($category->term_id); ?>" title="<?= $category->name; ?>">
				<?= $category->name; ?>
				<!-- <span class="badge"><?= $category->count; ?></span> -->
			</a>
		</li>
	<?php endforeach; ?>
</ul>