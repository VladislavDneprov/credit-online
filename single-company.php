<?php
/**
 * Template Name: Company
 * @package WordPress
 * @subpackage credit-online
 */
get_header(); // подключаем header.php ?>

<?php if ( have_posts() ) while ( have_posts() ) : the_post();?>
<?php
    $id = get_post_thumbnail_id(get_the_ID());
?>
<section class="company">
    <div class="wide-container">
        <?php get_template_part('part/breadcrumbs'); ?>
        <div class="about-block row">
            <div class="wide-container">
                <div style="background: url(<?= wp_get_attachment_image_src($id, 'full' )[0] ?>) no-repeat; background-size: contain;" class="label-block"></div>
                <p class="title">
                    <?php the_title(); ?>
                </p>
                <p><?php the_title(); ?> – <?= the_content(); ?></p>
                <h3>Условия оформления микрокредита на <?php the_title(); ?>:</h3>
                <p>
                    кредит можно взять на сумму до <?= get_post_meta(get_the_ID(), 'summ', 1); ?> гривен;
                    срок использования заемных средств варьируется от <?= get_post_meta(get_the_ID(), 'time_borrow_start', 1); ?> до <?= get_post_meta(get_the_ID(), 'time_borrow_finish', 1); ?> дней;<br>
                    стать клиентами могут только совершеннолетние граждане Украины;<br>
                    постоянные добросовестные заемщики имеют право оформить займ на сумму до 7500 гривен с 50%-ой скидкой;<br>
                    минимальная комиссия за пользованием кредитом составляет <?= get_post_meta(get_the_ID(), 'rate', 1); ?>%.<br>

                    Внимание! Компания <?php the_title(); ?> запустила акцию "Кредит под 0 %*", которая продлиться с 23 мая по 7 июня 2016 года. В рамках акции все новые
                    клиенты, которые впервые оформили заявку на получение займа от Moneyveo.ua, получают кредит под 0%*.<br>
                </p>
                <h3>Процедура оформления микрозайма на <?php the_title(); ?>:</h3>
                <p>
                    если вы хотите получить деньги в <?php the_title(); ?>, тогда, в первую очередь, следует определиться с суммой и периодом (при повторном обращении <br>
                    оформить заявку можно через личный кабинет);<br>
                    после этого следует оформить онлайн заявку на получение микрозайма;<br>
                    дождаться решения по заявке (рассмотрение запроса занимает <?= get_post_meta(get_the_ID(), 'time', 1); ?> часов);<br>
                    подтвердить права владения картой (сервис блокирует на несколько часов сумму до 1 гривны, а клиент должен отправить смс с уточнением <br>
                    суммы до копейки);<br>
                    получить денежные средства на свой счет.<br>
                </p>
                <div style="background: url(<?= wp_get_attachment_image_src($id, 'thumbnail' )[0] ?>) repeat-x left;" class="about-bottom"></div>
            </div>
        </div>
    </div>
</section>
<?php endwhile;?>
<section class="posts">
    <div class="wide-container">
        <?php get_kambeker(); ?>
    </div>
</section>
<section class="blog">
    <div class="blog-container">
        <div class="row">
            <div class="blog-block col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <div class="blog-content">
                    <p class="title">Запись в блоге 1</p>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer id accumsan massa. Donec suscipit commodo velit. Fusce pharetra mi sit amet felis auctor tempor. Donec vel odio enim, id ultricies odio. </p>
                    <a class="btn">Далее</a>
                </div>
            </div>

            <div class="blog-block col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <div class="blog-content">
                    <p class="title">Запись в блоге 1</p>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer id accumsan massa. Donec suscipit commodo velit. Fusce pharetra mi sit amet felis auctor tempor. Donec vel odio enim, id ultricies odio. </p>
                    <a class="btn">Далее</a>
                </div>
            </div>

            <div class="blog-block col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <div class="blog-content">
                    <p class="title">Запись в блоге 1</p>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer id accumsan massa. Donec suscipit commodo velit. Fusce pharetra mi sit amet felis auctor tempor. Donec vel odio enim, id ultricies odio. </p>
                    <a class="btn">Далее</a>
                </div>
            </div>

        </div>
    </div>
</section>

<?php get_footer(); ?>