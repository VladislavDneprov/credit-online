<!-- breadcrumbs -->
<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 breadcrumbs">
		<?php breadcrumbs(' /'); ?>
	</div>

	<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 list-category">
        <?php get_template_part('part/sidebar-blog'); ?>
    </div>
</div>