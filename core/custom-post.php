<?php
// подключаем функцию активации мета блока (my_extra_fields)
add_action('add_meta_boxes', 'my_extra_fields', 1);

function my_extra_fields() {
	add_meta_box( 'extra_fields', 'Настройки кредитной компании', 'fields_credit_company', 'company', 'normal', 'high'  );
}

// код блока
function fields_credit_company( $post ){
	?>

	<p>
		<label>Рейтинг</label>
		<select name="credit_company[starss]">
			<?php $sel_stars = get_post_meta($post->ID, 'starss', 1); ?>
			<option value="0" <?php selected( $sel_stars, '0' ); ?> >Нет рейтинга</option>
			<option value="1" <?php selected( $sel_stars, '1' ); ?> >1 звезда</option>
			<option value="2" <?php selected( $sel_stars, '2' ); ?> >2 звезды</option>
			<option value="3" <?php selected( $sel_stars, '3' ); ?> >3 звезды</option>
			<option value="4" <?php selected( $sel_stars, '4' ); ?> >4 звезды</option>
			<option value="5" <?php selected( $sel_stars, '5' ); ?> >5 звёзд</option>
		</select>
	</p>

	<p>
		<label>Время займа</label>
		<input type="number" name="credit_company[time_borrow_start]" min="1" placeholder="3" value="<?= get_post_meta($post->ID, 'time_borrow_start', 1); ?>" /> -
		<input type="number" name="credit_company[time_borrow_finish]" min="1" placeholder="4" value="<?= get_post_meta($post->ID, 'time_borrow_finish', 1); ?>" /> дня
	</p>
	
	<p>
		<label>Сумма кредита: </label>
		до <input type="number" name="credit_company[summ]" min="0" step="1" placeholder="5000 грн." value="<?= get_post_meta($post->ID, 'summ', 1); ?>" /> грн.

		<label>Ставка</label>
		<input type="number" name="credit_company[rate]" min="0" step="0.1"  placeholder="5%" value="<?= get_post_meta($post->ID, 'rate', 1); ?>" /> %
	</p>

	<p>
		<label>Время на расмотрение</label>
		<input type="time" name="credit_company[time]" value="<?= get_post_meta($post->ID, 'time', 1); ?>" />
	</p>

	<p>
		<label>Способ получения: </label>
			<?php
				$sel_get_way_1 = get_post_meta($post->ID, 'get_way_1', 1);
				$sel_get_way_2 = get_post_meta($post->ID, 'get_way_2', 1);
				$sel_get_way_3 = get_post_meta($post->ID, 'get_way_3', 1);
			 ?>
			<label><input type="checkbox" name="credit_company[get_way_1]" value="1" <?php checked($sel_get_way_1, '1'); ?> />Наличными через свои отделения</label>
			<label><input type="checkbox" name="credit_company[get_way_2]" value="2" <?php checked($sel_get_way_2, '2'); ?> />На карту Visa\MasterCard</label>
			<label><input type="checkbox" name="credit_company[get_way_3]" value="3" <?php checked($sel_get_way_3, '3'); ?> />Банковский счёт</label>
		</select>
	</p>

	<p>
		<label>Внешняя сылка</label>
		<input type="url" name="credit_company[url]" style="width:50%;" placeholder="http://credit-online.com" value="<?= get_post_meta($post->ID, 'url', 1); ?>" />
	</p>

	<p>
	<?php
			$pages = get_pages(array(
				'numberposts' 		=> -1,
				'post_status' 		=> 'publish',
				'parent'			=> get_page_by_path('country')->ID,
				'sort_column'		=> 'menu_order'
			));
	?>
		<label>Страна</label>
		<select name="credit_company[country]">
			<?php $sel_stars = get_post_meta($post->ID, 'country', 1); ?>
			<?php foreach ($pages as $key => $page): ?>
				<option value="<?= $key + 1; ?>" <?php selected( $sel_stars, $key + 1 ); ?> ><?= $page->post_title; ?></option>
			<?php endforeach; ?>
		</select>
	</p>

	<p>
		<label>Род занятий</label>
		<select name="credit_company[lessons]">
			<?php $sel_stars = get_post_meta($post->ID, 'lessons', 1); ?>
			<option value="1" <?php selected( $sel_stars, '1' ); ?> >Безработный</option>
			<option value="2" <?php selected( $sel_stars, '2' ); ?> >Пенсионерам</option>
			<option value="3" <?php selected( $sel_stars, '3' ); ?> >Студентам</option>
		</select>
	</p>

	<p>
		<label>Виды займов</label>
		<select name="credit_company[type]">
			<?php $sel_stars = get_post_meta($post->ID, 'type', 1); ?>
			<option value="1" <?php selected( $sel_stars, '1' ); ?> >До зарплаты</option>
			<option value="2" <?php selected( $sel_stars, '2' ); ?> >С плохой КИ</option>
			<option value="3" <?php selected( $sel_stars, '3' ); ?> >Без залога</option>
			<option value="4" <?php selected( $sel_stars, '4' ); ?> >Микрозаймы</option>
			<option value="5" <?php selected( $sel_stars, '5' ); ?> >Микрокредит</option>
			<option value="6" <?php selected( $sel_stars, '6' ); ?> >Без процентов</option>
			<option value="7" <?php selected( $sel_stars, '7' ); ?> >Без справки о доходах</option>
		</select>
	</p>

	<p>Лучший выбор: <?php $mark_the_best = get_post_meta($post->ID, 'the_best', 1); ?>
		<label><input type="radio" name="credit_company[the_best]" value="0" <?php checked( $mark_the_best, '0' ); ?> checked /> нет</label>
		<label><input type="radio" name="credit_company[the_best]" value="1" <?php checked( $mark_the_best, '1' ); ?> /> да</label>
	</p>
	
	<p>
		<?php $mark_the_best = get_post_meta($post->ID, 'top', 1); ?>
		<label> Топ: <input type="radio" name="credit_company[top]" value="0" <?php checked( $mark_the_best, '0' ); ?> checked /> нет</label>
		<label><input type="radio" name="credit_company[top]" value="1" <?php checked( $mark_the_best, '1' ); ?> /> да</label>
	</p>

	<input type="hidden" name="extra_fields_nonce" value="<?php echo wp_create_nonce(__FILE__); ?>" />

	<input name="save" type="submit" class="button button-primary button-large" value="Сохранить">

	<?php
}

// включаем обновление полей при сохранении
add_action('save_post', 'update_fields_credit_company', 0);

/* Сохраняем данные, при сохранении поста */
function update_fields_credit_company($post_id ) {
	if ( !isset($_POST['extra_fields_nonce']) || !wp_verify_nonce($_POST['extra_fields_nonce'], __FILE__) ) return false; // проверка
	if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE  ) return false; // если это автосохранение
	if ( !current_user_can('edit_post', $post_id) ) return false; // если юзер не имеет право редактировать запись

	if( !isset($_POST['credit_company']) ) return false;

	if(!isset($_POST['credit_company']['get_way_1']))
		delete_post_meta($post_id, 'get_way_1');
	if(!isset($_POST['credit_company']['get_way_2']))
		delete_post_meta($post_id, 'get_way_2');
	if(!isset($_POST['credit_company']['get_way_3']))
		delete_post_meta($post_id, 'get_way_3');

	// Все ОК! Теперь, нужно сохранить/удалить данные
	// $_POST['credit_company'] = array_map('trim', $_POST['extra']);
	foreach( $_POST['credit_company'] as $key=>$value ){
		if( empty($value) ){
			delete_post_meta($post_id, $key); // удаляем поле если значение пустое
			continue;
		}

		update_post_meta($post_id, $key, $value); // add_post_meta() работает автоматически
	}
	return $post_id;
}