<?php

require_once( ABSPATH . '/wp-admin/includes/taxonomy.php');

require(__DIR__.'/core/settings-themes.php');
require(__DIR__.'/core/custom-post.php');
require(__DIR__.'/core/breadcrumbs.php');
require(__DIR__.'/core/user_func.php');

register_post_type('company',
    array(
      	'labels' => array(
			'name'               => 'Кредитные компании', // основное название для типа записи
			'singular_name'      => 'Кредитная компания', // название для одной записи этого типа
			'add_new'            => 'Добавить кредитную компанию', // для добавления новой записи
			'add_new_item'       => 'Добавление новой кредитный компании', // заголовка у вновь создаваемой записи в админ-панели.
			'edit_item'          => 'Страница редактирования кредитной компании', // для редактирования типа записи
			'search_items'       => 'Найти кредитную компанию', // для поиска по этим типам записи
			'not_found'          => 'Кредитный компаний не найдено', // если в результате поиска ничего не было найдено
			'not_found_in_trash' => 'Кредитных компаний нет в корзине', // если не было найдено в корзине
			'menu_name'          => 'Кредитные компании', // название меню
		),
      'public' => true,
      'publicly_queryable' => true,
      'has_archive' => false,
      'menu_position'		=> 4,
      'menu_icon'           => 'dashicons-admin-multisite',
      'supports'            => array('title','editor', 'thumbnail', 'trackbacks', ), // 'title','editor','author','thumbnail','excerpt','trackbacks','custom-fields','comments','revisions','page-attributes','post-formats'
    )
);
register_post_type('slider',
    array(
      	'labels' => array(
			'name'               => 'Настройки слайдера', // основное название для типа записи
			'singular_name'      => 'Слайдер', // название для одной записи этого типа
			'add_new'            => 'Добавить слайдер', // для добавления новой записи
			'add_new_item'       => 'Добавление нового слайдера', // заголовка у вновь создаваемой записи в админ-панели.
			'edit_item'          => 'Страница редактирования слайдера', // для редактирования типа записи
			'search_items'       => 'Найти слайдер', // для поиска по этим типам записи
			'not_found'          => 'Слайдер не найден', // если в результате поиска ничего не было найдено
			'not_found_in_trash' => 'Слайдера нет в корзине', // если не было найдено в корзине
			'menu_name'          => 'Слайдер', // название меню
		),
      'public' => true,
      'publicly_queryable' => true,
      'has_archive' => false,
      'menu_position'		=> 5,
      'menu_icon'           => 'dashicons-format-gallery',
      'supports'            => array('title','editor', 'thumbnail', 'trackbacks', ), // 'title','editor','author','thumbnail','excerpt','trackbacks','custom-fields','comments','revisions','page-attributes','post-formats'
    )
);

add_filter('manage_company_posts_columns', 'new_company_col');

function new_company_col($defaults) {

	$columns = array(
		'cb'	 		=> '<input type="checkbox" />',
		'logo'			=>	'Логотип',
		'title'			=> 'Имя кредитной компании',
		'time'			=> 'Время на расмотрении',
		'summ' 			=> 'Сумма кредита',
		'date'			=>	'Date',
	);
	return $columns;
}

add_action('manage_company_posts_custom_column', 'get_item_id_pp', 10, 2);
 
// Вывод идентификаторов материалов
function get_item_id_pp($column, $post_id) {
    if($column == 'summ')
        echo  '<big><b>'.get_post_meta($post_id, 'summ', 1).' грн.</b></big>';

    if($column == 'time')
        echo  '<b>'.get_post_meta($post_id, 'time', 1).'</b> часов.';

    if($column == 'logo')
        echo  '<big><b>'.get_the_post_thumbnail($post_id, 'full', ['style' => 'width:80px; height: auto;']).'</b></big>';
}

function my_column_register_sortable( $columns )
{
	$columns['summ'] = 'summ';
	return $columns;
}

add_filter("manage_edit-company_sortable_columns", "my_column_register_sortable" );

/** add javascript **/
add_action('wp_enqueue_scripts', 'add_scripts');
function add_scripts() {
	
	/** lib **/
	wp_enqueue_script('jquery');
	wp_enqueue_script('bootstrap', 	get_template_directory_uri().'/js/lib/bootstrap.min.js', array('jquery'), '', true);
	wp_enqueue_script('slick', 		get_template_directory_uri().'/js/lib/slick/slick.min.js', array('jquery'), '', true);
	wp_enqueue_script('masked', 	get_template_directory_uri().'/js/lib/jquey.inputmask.bundle.min.js', array('jquery'), '', true);

	/** custom **/
	wp_enqueue_script('main', 		get_template_directory_uri().'/js/main.js', array('jquery'), '', true);

}

/** add css **/
add_action('wp_print_styles', 'add_styles');
function add_styles() {

	/** lib **/
	wp_enqueue_style( 'bootstrap', 		get_template_directory_uri().'/css/lib/bootstrap.min.css' );
	wp_enqueue_style( 'font-awesome', 	get_template_directory_uri().'/css/lib/font-awesome.min.css' );
	wp_enqueue_style( 'slick', 	get_template_directory_uri().'/js/lib/slick/slick.css' );
	wp_enqueue_style( 'slick-theme', 	get_template_directory_uri().'/js/lib/slick/slick-theme.css' );

	/** fonts **/
	wp_enqueue_style( 'fonts-pt-sans', 		'https://fonts.googleapis.com/css?family=PT+Sans' );
	wp_enqueue_style( 'fonts-open-sans', 	'https://fonts.googleapis.com/css?family=Open+Sans' );
	wp_enqueue_style( 'fonts-courgette', 	'https://fonts.googleapis.com/css?family=Courgette' );

	/** custom **/
	wp_enqueue_style( 'main', 			get_template_directory_uri().'/style.css' );
	wp_enqueue_style( 'global', 		get_template_directory_uri().'/css/global.css' );
	wp_enqueue_style( 'home', 			get_template_directory_uri().'/css/home.css' );
	wp_enqueue_style( 'page', 			get_template_directory_uri().'/css/page.css' );
	wp_enqueue_style( 'single', 		get_template_directory_uri().'/css/single.css' );
	wp_enqueue_style( 'page-blog', 		get_template_directory_uri().'/css/page-blog.css' );
	wp_enqueue_style( 'company', 		get_template_directory_uri().'/css/company.css' );
}

/** registration nav-menu **/
register_nav_menus([
	'top' 			=> 'Главное меню',
	'footer_1' 		=> 'Низ сайта, колонка 1',
	'footer_2' 		=> 'Низ сайта, колонка 2',
	'footer_3' 		=> 'Низ сайта, колонка 3'
]);

$category_blog = wp_insert_category(array(
  	'cat_name' 					=> 'Блог',
  	'category_nicename' 		=> 'blog',
  	'taxonomy' 					=> 'category'
));


add_theme_support('post-thumbnails'); # add thubnails

remove_filter('the_content', 'wpautop');