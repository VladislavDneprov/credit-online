<!DOCTYPE html>
<html lang="ru">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Bootstrap 101 Template</title>

        <?php wp_head(); ?>
    </head>
    <body>
	    <header id="top">
	    	<?= wp_nav_menu(array('theme_location' => 'top', 'menu_class' => 'nav-menu col-lg-4 col-lg-offset-7 col-md-5 col-md-offset-6 col-sm-12 col-xs-12','container' => 'ul')); ?>
	    	<a href="<?= get_home_url(); ?>" class="logo"></a>	    	
	    </header>
		<section class="top-slider"> 
			<div class="top-slider-img">
				<img src="<?= get_template_directory_uri().'/img/top-slider/1.jpg'?>">
			</div>
			<div class="slider-container">	
		    	<div class="wide-container">	      		
		      		<div class="slider">
		      			<?php
								      				$args = array(
								'numberposts'     => -1,
								'offset'          => 0,
								'orderby'         => 'post_date',
								'order'           => 'DESC',
								'include'         => '',
								'exclude'         => '',
								'post_type'       => 'slider',
								'post_parent'     => '',
								'post_status'     => 'publish'
							);

							$slider = get_posts($args);
							foreach ($slider as $key => $value):?>
				       			<div class="item">
				       				<div class="desc-content">
					       				<div class="desc-img col-lg-4 col-md-5 col-sm-12 col-xs-12 center-sm">
					       					<?=get_the_post_thumbnail($value->ID);?>
					       				</div>
					       				<div class="desc-text col-lg-7 col-md-7 col-sm-12 col-xs-12">
					       					<p class="title"><?=$value->post_title;?></p>
					       					<p><?=$value->post_content;?></p>					       					
					       				</div>
					       			</div>
				       			</div>
		       				<?php endforeach; ?>
		       		</div>
		       	</div>
	       	</div>	 	  	
		</section>
		<section class="filters">
			<div class="wide-container">	
				<div class="row">		
					<?php $filter = get_option('filter_option');?>
					<div class="filter-block col-lg-3 col-md-3 col-sm-4 col-xs-6">
						<label><?=$filter['input1'];?></label>
						<button class="dropdown styled-select" data-toggle="dropdown"><?=$filter['textarea1'];?>
						</button>
						<?php
							$pages = get_pages(array(
								'numberposts' 		=> -1,
								'post_status' 		=> 'publish',
								'parent'			=> get_page_by_path('getway')->ID,
								'sort_column'		=> 'menu_order'
							));
						?>
						<ul class="dropdown-menu">
							<?php foreach ($pages as $key => $page): ?>
								<li>
									<form action="<?= get_permalink($page->ID); ?>" method="post">
										<input type="hidden" value="<?= $key+1 ?>" name="value" />
										<input type="hidden" value="get_way_<?= $key+1 ?>" name="key" />
										<input type="hidden" value="1" name="filter" />
										<button class="btn-filter" type="submit"><?= $page->post_title; ?></button>
									</form>
								</li>
							<?php endforeach; ?>
						</ul>
					</div>

					<div class="filter-block col-lg-3 col-md-3 col-sm-4 col-xs-6">
						<label><?=$filter['input2'];?></label>
						<button class="dropdown styled-select" data-toggle="dropdown"><?=$filter['textarea2'];?>
						</button>
						<?php
							$pages = get_pages(array(
								'numberposts' 		=> -1,
								'post_status' 		=> 'publish',
								'parent'			=> get_page_by_path('lessons')->ID,
								'sort_column'		=> 'menu_order'
							));
						?>
						<ul class="dropdown-menu">
							<?php foreach ($pages as $key => $page): ?>
								<li>
									<form action="<?= get_permalink($page->ID); ?>" method="post">
										<input type="hidden" value="<?= $key+1 ?>" name="value" />
										<input type="hidden" value="lessons" name="key" />
										<input type="hidden" value="2" name="filter" />
										<button class="btn-filter" type="submit"><?= $page->post_title; ?></button>
									</form>
								</li>
							<?php endforeach; ?>
						</ul>
					</div>

					<div class="filter-block col-lg-3 col-md-3 col-sm-4 col-xs-6">
						<label><?=$filter['input3'];?></label>
						<button class="dropdown styled-select" data-toggle="dropdown"><?=$filter['textarea3'];?>
						</button>
						<?php
							$pages = get_pages(array(
								'numberposts' 		=> -1,
								'post_status' 		=> 'publish',
								'parent'			=> get_page_by_path('typeget')->ID,
								'sort_column'		=> 'menu_order'
							));
						?>
						<ul class="dropdown-menu">
							<?php foreach ($pages as $key => $page): ?>
								<li>
									<form action="<?= get_permalink($page->ID); ?>" method="post">
										<input type="hidden" value="<?= $key+1 ?>" name="value" />
										<input type="hidden" value="type" name="key" />
										<input type="hidden" value="3" name="filter" />
										<button class="btn-filter" type="submit"><?= $page->post_title; ?></button>
									</form>
								</li>
							<?php endforeach; ?>
						</ul>
					</div>

					<div class="filter-block col-lg-3 col-md-3 col-sm-4 col-xs-6">
						<label><?=$filter['input4'];?></label>
						<button class="dropdown styled-select" data-toggle="dropdown"><?=$filter['textarea4'];?>
						</button>
						<?php
							$pages = get_pages(array(
								'numberposts' 		=> -1,
								'post_status' 		=> 'publish',
								'parent'			=> get_page_by_path('country')->ID,
								'sort_column'		=> 'menu_order'
							));
						?>
						<ul class="dropdown-menu">
							<?php foreach ($pages as $key => $page): ?>
								<li>
									<form action="<?= get_permalink($page->ID); ?>" method="post">
										<input type="hidden" value="<?= $key+1 ?>" name="value" />
										<input type="hidden" value="country" name="key" />
										<input type="hidden" value="2" name="filter" />
										<button class="btn-filter" type="submit"><?= $page->post_title; ?></button>
									</form>
								</li>
							<?php endforeach; ?>
						</ul>
					</div>	
				</div>
			</div>
		</section>