<?php
/**
 * Template Name: Home
 * @package WordPress
 * @subpackage credit-online
 */
get_header(); ?>


<section class="posts">
	<div class="wide-container row">
		<?php get_credit_company(); ?>
	</div>
</section>
<section class="blog">
	<div class="blog-container" style="padding:50px;">
		<div class="row">
			<div class="col-md-12">
				<?= get_page_by_path('main')->post_content ?>
			</div>
		</div>
	</div>
</section>
<section class="blog">
	<div class="blog-container">
		<div class="row">
			<?php get_blog_block(); ?>
		</div>
	</div>
</section>

<?php get_footer(); ?> 