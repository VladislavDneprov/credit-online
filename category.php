<?php
	/**
	 * get_template_part('part/sidebar-blog') - get sidebar blog
	 *
	 * breadcrumbs() - get breadcrumbs
	 *
	 **/
?>

<?php get_header(); ?>

<section class="category">
	<div class="container">
		
		<?php get_template_part('part/breadcrumbs'); ?>

		<div class="row category-post">
				<?php while (have_posts()) : the_post(); ?>
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 category-post-block">
					<div class="row">
						<div class="col-md-3 img"><?php the_post_thumbnail(); ?></div>
						<div class="col-md-9">
							<div class="title"><a href="<?= get_permalink(); ?>"><?php the_title(); ?></a></div>
							<div class="desc"><?= crop_string(get_the_content(), 300, '...'); ?></div>
							<div class="more text-right"><a class="btn" href="<?= get_permalink(); ?>" title="<?php the_title(); ?>">Далее</a></div>
						</div>
					</div>
				</div>
				<?php endwhile; ?>
		</div>

	</div>
</section>
<?php get_footer(); ?> 