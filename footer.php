<?php
/**
 * Шаблон подвала (footer.php)
 * @package WordPress
 * @subpackage your-clean-template-3
 */
$link = get_option('main_option');?>
	<footer>
		<div class="footer-top">
			<div class="container">
				<div class="row">
					<div class="col-lg-2 col-md-2 col-sm-4 lol-xs-12">
						<h4>Ссылки</h4>
						<?= wp_nav_menu(array('theme_location' => 'footer_1')); ?>
					</div>
					<div class="col-lg-2 col-md-2 col-sm-4 lol-xs-12">
						<h4>Ссылки</h4>
						<?= wp_nav_menu(array('theme_location' => 'footer_2')); ?>
					</div>
					<div class="col-lg-2 col-md-2 col-sm-4 lol-xs-12">
						<h4>Ссылки</h4>
						<?= wp_nav_menu(array('theme_location' => 'footer_3')); ?>
					</div>
					<div class="col-lg-3 col-md-3 col-sm-6 lol-xs-12">
						<h4>Мы в соц. сетях</h4>
						<div class="social-conteiner">
							<div class="footer-social twitter"><a href="<?=$link['Twitter'];?>"><img src="<?= get_template_directory_uri().'/img/twitter.png' ?>"></a></div>
							<div class="footer-social linkedin"><a href="<?=$link['Linkedin'];?>"><img src="<?= get_template_directory_uri().'/img/linkedin.png' ?>"></a></div>
							<div class="footer-social facebook"><a href="<?=$link['Facebook'];?>"><img src="<?= get_template_directory_uri().'/img/facebook.png' ?>"></a></div>
							<div class="footer-social e-mail"><a href="mailto:<?=$link['email']?>"><img src="<?= get_template_directory_uri().'/img/e-mail.png' ?>"></a></div>
						</div>
					</div>
					<div class="col-lg-3 col-md-3 col-sm-6 lol-xs-12 footer-contact-form">
						<h4>Задать вопрос</h4>
						<?php echo do_shortcode('[contact-form-7 id="165" title="Связаться с нами"]'); ?>
					</div>
					
				</div>
			</div>
		</div>
		<div class="footer-bottom">
			<div class="container">
				<div class="row">
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 text-center-sm footer-bootom-copyright">
						Все права защищены с <a href="http://moneyonline.com.ua">MoneyOnline.com.ua</a>
					</div>
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 text-center-sm footer-bootom-developer">
					
					</div>
				</div>
			<div>
		</div>
	</footer>
	<!--<button type="button" class="btn" data-toggle="modal" data-target="#modal-win-advertisment">
	  Launch demo modal
	</button>-->
	<div class="modal fade" tabindex="-1" role="dialog" id="modal-win-advertisment">
		<div class="modal-dialog modal-win-advertisment" role="document">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<div class="modal-header">					
				<h4 class="modal-title">Многие наши клиенты берут кредит здесь:</h4>
			</div>
			<div class="modal-content">
				<div class="lamp"></div>				
				<div class="modal-body">
					<?php $kambeker = get_option('kambeker_option'); $list_kambeker = array($kambeker['select_1'], $kambeker['select_2'] );?>
					<?php foreach ($list_kambeker as $value):?>
					<div class="post-block row <?php echo (get_post_meta($value, 'top', 1) ? 'top' : ''); ?> <?php echo (get_post_meta($value, 'the_best', 1) ? 'best' : ''); ?>">
			
						<div class="post-img col-lg-4 col-md-4 col-sm-4 col-xs-6 center-sm">
							<?= get_the_post_thumbnail($value); ?>
						</div>
						<div class="post-content col-lg-4 col-md-4 col-sm-4 col-xs-6">
							<ul>
								<li>
									<p>Рейтинг: <span>2.2</span></p>
								</li>
								<li>
									<p>Cрок: <span><?= get_post_meta($value, 'time_borrow_start', 1); ?> - <?= get_post_meta($value, 'time_borrow_finish', 1); ?> дня</span> </p>
									
								</li>
								<li>
									<p>Сумма: <span>до <?= get_post_meta($value, 'summ', 1); ?></span></p>
									
								</li>
								<li>
									<p>Ставка <span><?= get_post_meta($value, 'rate', 1); ?>%/день</span></p>
									
								</li>
								<li>
									<p>Рассмотрение <span><?= get_post_meta($value, 'time', 1); ?> часов</span></p>
									
								</li>
							</ul>
						</div>
						<div class="post-order col-lg-4 col-md-4 col-sm-4 col-xs-12">
							<a target="blank" href="<?= get_post_meta($value, 'url', 1); ?>" title="Перейти на сайт">
								<p>
									Взять<br>кредит!
								</p>
							</a>
						</div>
					</div>
				<?php endforeach; ?>
				</div>
				
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div><!-- /.modal -->
	<a id="toTop" href="#top"><i class="fa fa-arrow-up"></i></a>
<?php wp_footer(); // необходимо для работы плагинов и функционала  ?>
</body>
</html>