<?php

/** user functions **/
function crop_string($string, $count, $add_symbol = '')
{  
   return substr($string, 0, $count).((strlen($string) > $count) ? $add_symbol : '');
}

function get_credit_company() {

	$meta_key = $_POST['key'];
	$meta_value = $_POST['value'];

	$args = array(
		'numberposts'     => 5,
		'offset'          => 0,
		'orderby'         => 'post_date',
		'order'           => 'DESC',
		'include'         => '',
		'exclude'         => '',
		'meta_key'        => ($meta_key ? $meta_key : ''),
		'meta_value'      => ($meta_value ? $meta_value : ''),
		'post_type'       => 'company',
		'post_parent'     => '',
		'post_status'     => 'publish'
	);

	$categories = get_posts($args);

	foreach ($categories as $key => $post): ?>
		
		<div class="post-block <?php echo (get_post_meta($post->ID, 'top', 1) ? 'top' : ''); ?> <?php echo (get_post_meta($post->ID, 'the_best', 1) ? 'best' : ''); ?>">
			
				<div class="post-img col-lg-3 col-md-4 col-sm-12 col-xs-12 center-sm">
					<a title="Читать о компании..." href="<?= get_permalink($post->ID); ?>" title=""><?= get_the_post_thumbnail($post->ID); ?></a>
				</div>
				<div class="post-content col-lg-7 col-md-6 col-sm-12 col-xs-12">
					<ul>
						<li>
							<p>Рейтинг</p>
							<p>
							<?php if(get_post_meta($post->ID, 'starss', 1)) : ?>
								<?php for($i=1; $i < get_post_meta($post->ID, 'starss', 1); $i++): ?>
									<i class="fa fa-star"></i>
								<?php endfor; ?>
								<?php else: ?>
									Нет рейтинга
							<?php endif; ?>
							</p>
						</li>
						<li>
							<p>На срок</p>
							<p>
								<?php if(get_post_meta($post->ID, 'time_borrow_start', 1)) : ?>
									<?= get_post_meta($post->ID, 'time_borrow_start', 1); ?> - <?= get_post_meta($post->ID, 'time_borrow_finish', 1); ?> дня
								<?php else: ?>
										Не указан
								<?php endif; ?>
							</p>
						</li>
						<li>
							<p>Сумма</p>
							<p>
								<?php if(get_post_meta($post->ID, 'summ', 1)) : ?>
									<?= get_post_meta($post->ID, 'summ', 1); ?>
								<?php else: ?>
										Не указана
								<?php endif; ?>
							</p>
						</li>
						<li>
							<p>Ставка</p>
							<p>
								<?php if(get_post_meta($post->ID, 'rate', 1)) : ?>
									<?= get_post_meta($post->ID, 'rate', 1); ?>%/день
								<?php else: ?>
										Не указан
								<?php endif; ?>
							</p>
						</li>
						<li>
							<p>Рассмотрение</p>
							<p>
								<?php if(get_post_meta($post->ID, 'time', 1)) : ?>
									<?= get_post_meta($post->ID, 'time', 1); ?> часов
								<?php else: ?>
										Не указаны
								<?php endif; ?>
							</p>
						</li>
						<li>
							<p>Получение</p>
							<?php if(get_post_meta($post->ID, 'get_way_1', 1)) : ?><img src="<?= get_template_directory_uri().'/img/shit2.png' ?>"><?php endif; ?>
							<?php if(get_post_meta($post->ID, 'get_way_2', 1)) : ?><img src="<?= get_template_directory_uri().'/img/shit1.png' ?>"><?php endif; ?>
						</li>
					</ul>
					<p class="attraction"><?= $post->post_title; ?></p>
				</div>
				<div class="post-order col-lg-2 col-md-2 col-sm-12 col-xs-12">
					<a target="blank" href="<?= get_post_meta($post->ID, 'url', 1); ?>" title="Перейти на сайт">
						<p>
							Взять<br>кредит!
						</p>
					</a>
				</div>
			</div>

	<?php endforeach;

}

function get_blog_block() {

$blog_category = get_category_by_slug('blog');

	$args = array(
		'numberposts'     => 3,
		'offset'          => 0,
		'category'        => $blog_category->term_id,
		'orderby'         => 'post_date',
		'order'           => 'DESC',
		'include'         => '',
		'exclude'         => '',
		'meta_key'        => '',
		'meta_value'      => '',
		'post_type'       => 'post',
		'post_parent'     => '',
		'post_status'     => 'publish'
	);

	$categories = get_posts($args);

	foreach ($categories as $key => $post): ?>
		
			<div class="blog-block col-lg-4 col-md-4 col-sm-6 col-xs-12">
				<div class="blog-content">
					<p class="title"><?= $post->post_title; ?></p>
					<p><?= crop_string($post->post_content, 100); ?></p>
					<a href="<?= get_permalink($post->ID); ?>" class="btn" title="Подробнее">Далее</a>
				</div>
			</div>

	<?php endforeach;

}

function get_kambeker() {
			?>
				<div class="post-on-single">
				<?php $kambeker = get_option('kambeker_option'); $list_kambeker = array($kambeker['select_1'], $kambeker['select_2'] );?>
				<?php foreach ($list_kambeker as $value):?>
				<div class="post-block row <?php echo (get_post_meta($value, 'top', 1) ? 'top' : ''); ?> <?php echo (get_post_meta($value, 'the_best', 1) ? 'best' : ''); ?>">
			
				<div class="post-img col-lg-3 col-md-4 col-sm-12 col-xs-12 center-sm">
					<a title="Читать о компании..." href="<?= get_permalink($value); ?>" title=""><?= get_the_post_thumbnail($value); ?></a>
				</div>
				<div class="post-content col-lg-7 col-md-6 col-sm-12 col-xs-12">
					<ul>
						<li>
							<p>Рейтинг</p>
							<p>
							<?php if(get_post_meta($value, 'starss', 1)) : ?>
								<?php for($i=1; $i < get_post_meta($value, 'starss', 1); $i++): ?>
									<i class="fa fa-star"></i>
								<?php endfor; ?>
								<?php else: ?>
									Нет рейтинга
							<?php endif; ?>
							</p>
						</li>
						<li>
							<p>На срок</p>
							<p>
								<?php if(get_post_meta($value, 'time_borrow_start', 1)) : ?>
									<?= get_post_meta($value, 'time_borrow_start', 1); ?> - <?= get_post_meta($value, 'time_borrow_finish', 1); ?> дня
								<?php else: ?>
										Не указан
								<?php endif; ?>
							</p>
						</li>
						<li>
							<p>Сумма</p>
							<p>
								<?php if(get_post_meta($value, 'summ', 1)) : ?>
									<?= get_post_meta($value, 'summ', 1); ?>
								<?php else: ?>
										Не указана
								<?php endif; ?>
							</p>
						</li>
						<li>
							<p>Ставка</p>
							<p>
								<?php if(get_post_meta($value, 'rate', 1)) : ?>
									<?= get_post_meta($value, 'rate', 1); ?>%/день
								<?php else: ?>
										Не указан
								<?php endif; ?>
							</p>
						</li>
						<li>
							<p>Рассмотрение</p>
							<p>
								<?php if(get_post_meta($value, 'time', 1)) : ?>
									<?= get_post_meta($value, 'time', 1); ?> часов
								<?php else: ?>
										Не указаны
								<?php endif; ?>
							</p>
						</li>
						<li>
							<p>Получение</p>
							<?php if(get_post_meta($value, 'get_way_1', 1)) : ?><img src="<?= get_template_directory_uri().'/img/shit2.png' ?>"><?php endif; ?>
							<?php if(get_post_meta($value, 'get_way_2', 1)) : ?><img src="<?= get_template_directory_uri().'/img/shit1.png' ?>"><?php endif; ?>
						</li>
					</ul>
					<p class="attraction"><?=get_the_title($value); ?></p>
				</div>
				<div class="post-order col-lg-2 col-md-2 col-sm-12 col-xs-12">
					<a target="blank" href="<?= get_post_meta($value, 'url', 1); ?>" title="Перейти на сайт">
						<p>
							Взять<br>кредит!
						</p>
					</a>
				</div>
			</div>
	<?php endforeach;?>
			</div>
<?php }

