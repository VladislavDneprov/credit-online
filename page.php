<?php
/**
 * Template Name: Home
 * @package WordPress
 * @subpackage credit-online
 */


$current_id = get_queried_object_id();


get_header(); 

// var_dump($_POST);

if ( have_posts() ) while ( have_posts() ) : the_post();

$filter  = array(
	1 => get_option('filter_option')['input1'], 
	2 => get_option('filter_option')['input2'],
	3 => get_option('filter_option')['input3'],
	4 => get_option('filter_option')['input4']
);

?>


<section class="posts">
	<div class="wide-container row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 breadcrumbs" style="padding-top:0;">
			<?php breadcrumbs(' /'); ?>
		</div>
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<div style="color: #718a39; font-family: MyRiadPro; font-size: 1.2em;"><h1 style="display:inline-block; color: #718a39; font-family: MyRiadPro; font-size: 1.8em;"><?php the_title(); ?></h1></div>
			<?php get_credit_company(); ?>
		</div>
	</div>
</section>

<section class="blog">
	<div class="blog-container">
		<div class="row">
			<?php get_blog_block(); ?>
		</div>
	</div>
</section>

<?php $post = get_post($current_id); ?>

<section class="blog">
	<div class="blog-container" style="padding:50px;">
		<div class="row">
			<div class="col-md-12">
					<article id="post-<?php the_ID(); ?>">
						<?php the_content(); ?>
					</article>
			</div>
		</div>
	</div>
</section>

<?php endwhile; ?>

<?php get_footer(); ?> 