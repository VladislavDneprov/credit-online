jQuery(function($){

	function Menu(){
		var top = $(document).scrollTop();
		if (top < 800) {
			$("#toTop").removeClass('float');							
		}				
		else {
			$("#toTop").addClass('float');						
		}
	}

	$(document).ready(function(){

		$('#toTop').click(function(){
	        var el = $(this).attr('href');
	        $('body').animate({
	            scrollTop: $(el).offset().top}, 700);
	        return false; 
		});

		Menu();
			
		$(window).scroll(function() {
			Menu();
		});

		$('.slider').slick({
			slidesToShow: 1,
		    slidesToScroll: 1,
		    autoplay: false,
		    autoplaySpeed: 1500,
		    dots: false,
		    infinite: true,
		    arrows: true,
		   	nextArrow: '<div class="slider-arrows next"></div>',
		   	prevArrow: '<div class="slider-arrows prev"></div>',
		    responsive: [
			    {
			      breakpoint: 1199,
			      settings: {
			        slidesToShow: 1,
			        slidesToScroll: 1,
			        infinite: true,
			      }
			    },
			    
			    
	    	]
		});
		setTimeout(function(){
			$('#modal-win-advertisment').modal('show')
		}, 30000);	

		$('.email').inputmask({
			mask: "*{2,}@${2,}.a{2,3}",
			definitions: {
		    	'*': {
		        	validator: "[0-9A-Za-z\._\-]",		       
		      	},
		      	'$': {
		      		validator: "[0-9A-Za-z_\-]",		
		      	}
		    }
		});

		$('.tel').inputmask({
			mask: "9{0,13}"
		});	
		$('.message').inputmask({
			mask:'*{0,50}',
			definitions: {
		    	'*': {
		        	validator: "^[a-zA-Z0-9!@#\$%\^\&*\)\(+=._-№]+$",		       
		      	}
		    }
		});
	});

	/*$('.styled-select').find('select').change(function(){
		console.log($(this).val());
		window.location.href = $(this).val();
		//$(location).attr('href', $(this).val());
	});*/
})